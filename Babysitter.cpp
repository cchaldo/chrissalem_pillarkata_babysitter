#include "Babysitter.h"

//default constructor
Babysitter::Babysitter() 
	: start_time(-1), end_time(-1), earnings(0), bedtime(20), twelveAnHour(12), eightAnHour(8), sixteenAnHour(16) {}

//set the hour that the babysitter starts work
void Babysitter::setStartTime(int time_in) { start_time = time_in; }

//esnure that the babysitter has chosen a valid hour to start working
bool Babysitter::verifyStartTime()
{
	if (start_time > 4 && start_time < 17)
		return false;

	return true;
}

//set the hour that the babysitter stops work
void Babysitter::setEndTime(int time_in) { end_time = time_in; }

//esnure that the babysitter has chosen a valid hour to stop working
bool Babysitter::verifyEndTime()
{
	bool startTimeInitialized = (start_time != -1);

	if (end_time > 4 && end_time < 17)
		return false;

	//end time is now between 17-4

	
	if (end_time < start_time && startTimeInitialized)
		if (!(end_time < 17 && start_time > 4))
			return false;
	
	if (start_time < 17 && end_time > 4 && startTimeInitialized)
		return false;

	return true;
}

//ensure that the babysitter does not enter an invalid hour
bool Babysitter::timeInputIsBetweenZeroAndTwentyThree(int time_in)
{
	if (time_in > 23 || time_in < 0)
		return false;

	return true;
}

int Babysitter::calculateEarnings()
{
	int temp = start_time;
	while (temp != end_time)
	{
		if (temp >= 17 && temp < 20)
			earnings += twelveAnHour;
		else if (temp >= 20 && temp <= 23)
			earnings += eightAnHour;
		else if (temp >= 0 && temp < 4)
			earnings += sixteenAnHour;

		temp++;
		temp %= 24;
	}

	return earnings;
}



