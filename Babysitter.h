
class Babysitter
{
	int start_time;
	int end_time;
	int earnings;

	const int bedtime, twelveAnHour, eightAnHour, sixteenAnHour;

public:
	Babysitter();

	void setStartTime(int);
	bool verifyStartTime();
	int  getStartTime() { return start_time; }

	void setEndTime(int);
	bool verifyEndTime();
	int  getEndTime() { return end_time; }

	bool timeInputIsBetweenZeroAndTwentyThree(int);

	int calculateEarnings();
};