#Welcome to my implementation of the Babysitter Kata

####To run the application:
[Clone](https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html) this repository. Then open up a terminal window and navigate to the directory that you cloned into. At the command prompt, type:

```bash
make run
```

and press Enter. Then follow the prompts as if you were a babysitter looking to work a shift tonight ! Enjoy.

####If you are interested in running the test suite:
At the command prompt, type:

```bash
make runtests
```

and press Enter. 

####If you want to remove the executables created by the above commands:
At the command prompt, type:

```bash
make clean
```

and press Enter.


##Some details about the environment I used:

* **OS**: Red Hat Linux version 3.10.0
* **Language**: C++
* **Compiler**: g++ version 4.8.5

I am sure that compiling with other versions of g++ on other operating systems will work just fine, but if you are experiencing any issues, please feel free to reach out to me: <cchaldo@umich.edu>

--Chris Salem