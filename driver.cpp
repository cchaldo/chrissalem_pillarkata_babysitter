#include <iostream>
#include "Babysitter.h"

using namespace std;

void printStartMessage();
bool getUserInput(Babysitter&);
void printEarnings(Babysitter&);

int main()
{
	Babysitter b;

	printStartMessage();
	if (!getUserInput(b)) 
		return 1;
	printEarnings(b);

	return 0;
}

void printStartMessage()
{
	cout << "\nWelcome, Babysitter !\n\n"
		 << "Note some of the rules you must follow to use this application:\n"
		 << "- You may start no earlier than 5:00PM\n"
		 << "- You may leave no later than 4:00AM\n"
		 << "- You will get paid $12/hour from start-time to bedtime (5:00PM-8:00PM)\n"
		 << "- You will get paid $8/hour from bedtime to midnight (8:00PM-12:00AM)\n"
		 << "- You will get paid $16/hour from midnight to end of job (12:00AM-4:00AM)\n"
		 << "- You will get paid for full hours (no fractional hours)\n\n";
}

bool getUserInput(Babysitter& b)
{
	int time_in;

	//START TIME
	cout << "Please input your start time as an integer in military time (hours 0-23): ";
	cin >> time_in;
	if (!b.timeInputIsBetweenZeroAndTwentyThree(time_in))
	{
		cerr << "ERROR: You entered an invalid time\n";
		return false;
	}
	b.setStartTime(time_in);
	if (!b.verifyStartTime())
	{
		cerr << "ERROR: You must enter a start time between 5:00PM and 4:00AM (17:00-04:00)\n";
		return false;
	}

	//END TIME
	cout << "Please input your end time as an integer in military time (hours 0-23): ";
	cin >> time_in;
	if (!b.timeInputIsBetweenZeroAndTwentyThree(time_in))
	{
		cerr << "ERROR: You entered an invalid time\n";
		return false;
	}
	b.setEndTime(time_in);
	if (!b.verifyEndTime())
	{
		cerr << "ERROR: You must enter an end time between 5:00PM and 4:00AM (17:00-04:00) "
			 << "and your end time must be at or after your start time\n";
		return false;
	}

	return true;
}

void printEarnings(Babysitter& b)
{
	if (b.getStartTime() == b.getEndTime())
	{
		cout << "\nLooks like you don't want to work tonight !\n";
		return;
	}

	cout << "\nYou will be working from " << b.getStartTime() << ":00 to " << b.getEndTime() << ":00 "
		 << "and will make $" << b.calculateEarnings()
		 << "\n\nHave a great shift !\n";
}

