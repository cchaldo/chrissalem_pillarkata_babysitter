#include <cassert>
#include <iostream>
#include "Babysitter.h"

using namespace std;

void theBabysitterCanStartAtFivePM()
{
	cout << "----Test 'theBabysitterCanStartAtFivePM'----\n";
	cout << "Testing that the babysitter can start working at 5:00PM\n";
	Babysitter b;
	b.setStartTime(17);
	assert(b.verifyStartTime() == true);
	cout << "Test 'theBabysitterCanStartAtFivePM' passed !\n\n";
}

void theBabysitterCannotStartAtFourPM()
{
	cout << "----Test 'theBabysitterCannotStartAtFourPM'----\n";
	cout << "Testing that the babysitter cannot start working at 4:00PM\n";
	Babysitter b;
	b.setStartTime(16);
	assert(b.verifyStartTime() == false);
	cout << "Test 'theBabysitterCannotStartAtFourPM' passed !\n\n";
}

void theBabysitterCannotStartBetweenFiveAMAndFourPM()
{
	cout << "----Test 'theBabysitterCannotStartBetweenFiveAMAndFourPM'----\n";
	cout << "Testing that the babysitter cannot start working between 5:00AM and 4:00 PM\n";
	Babysitter b;
	for (int i = 5; i < 17; ++i)
	{
		b.setStartTime(i);
		assert(b.verifyStartTime() == false);
	}
	cout << "Test 'theBabysitterCannotStartBetweenFiveAMAndFourPM' passed !\n\n";
}

void theBabysitterCanStartBetweenFivePMAndFourAM()
{
	cout << "----Test 'theBabysitterCanStartBetweenFivePMAndFourAM'----\n";
	cout << "Testing that the babysitter can start working between 5:00PM and 4:00 AM\n";
	Babysitter b;

	cout << "The babysitter can successfully start at these times: ";
	for (int i = 17; i < 24; ++i)
	{
		cout << i << ' ';
		b.setStartTime(i);
		assert(b.verifyStartTime() == true);
	}
	for (int i = 0; i < 5; ++i)
	{
		cout << i << ' ';
		b.setStartTime(i);
		assert(b.verifyStartTime() == true);
	}
	cout << "Test 'theBabysitterCanStartBetweenFivePMAndFourAM' passed !\n\n";
}

void theBabysitterCanLeaveAtFourAM()
{
	cout << "----Test 'theBabysitterCanLeaveAtFourAM'----\n";
	cout << "Testing that the babysitter can stop working at 4:00AM\n";
	Babysitter b;
	b.setEndTime(4);
	assert(b.verifyEndTime() == true);
	cout << "Test 'theBabysitterCanLeaveAtFourAM' passed !\n\n";
}

void theBabysitterCannotLeaveAtFiveAM()
{
	cout << "----Test 'theBabysitterCannotLeaveAtFiveAM'----\n";
	cout << "Testing that the babysitter cannot stop working at 5:00AM\n";
	Babysitter b;
	b.setEndTime(5);
	assert(b.verifyEndTime() == false);
	cout << "Test 'theBabysitterCannotLeaveAtFiveAM' passed !\n\n";
}

void theBabysitterCannotLeaveBetweenFiveAMAndFourPM()
{
	cout << "----Test 'theBabysitterCannotLeaveBetweenFiveAMAndFourPM'----\n";
	cout << "Testing that the babysitter cannot stop working between 5:00AM and 4:00PM\n";
	Babysitter b;
	for (int i = 5; i < 17; ++i)
	{
		b.setEndTime(i);
		assert(b.verifyEndTime() == false);
	}
	cout << "Test 'theBabysitterCannotLeaveBetweenFiveAMAndFourPM' passed !\n\n";
}

void theBabysitterCanLeaveBetweenFivePMAndFourAM()
{
	cout << "----Test 'theBabysitterCanLeaveBetweenFivePMAndFourAM'----\n";
	cout << "Testing that the babysitter can stop working between 5:00PM and 4:00AM\n";
	Babysitter b;

	cout << "The babysitter can successfully leave at these times: ";
	for (int i = 17; i < 24; ++i)
	{
		cout << i << ' ';
		b.setEndTime(i);
		assert(b.verifyEndTime() == true);
	}
	for (int i = 0; i < 5; ++i)
	{
		cout << i << ' ';
		b.setEndTime(i);
		assert(b.verifyEndTime() == true);
	}
	cout << "Test 'theBabysitterCanLeaveBetweenFivePMAndFourAM' passed !\n\n";
}

void theBabysitterCannotLeaveBeforeStartingWithValidTimesBeforeMidnight()
{
	cout << "----Test 'theBabysitterCannotLeaveBeforeStartingWithValidTimesBeforeMidnight'----\n";
	cout << "Testing that the babysitter cannot end a shift before beginning one at times before midnight\n";
	Babysitter b;

	b.setStartTime(18);
	b.setEndTime(17);

	assert(b.verifyEndTime() == false);
	cout << "Test 'theBabysitterCannotLeaveBeforeStartingWithValidTimesBeforeMidnight' passed !\n\n";
}

void theBabysitterCannotLeaveBeforeStartingWithValidTimesAfterMidnight()
{
	cout << "----Test 'theBabysitterCannotLeaveBeforeStartingWithValidTimesAfterMidnight'----\n";
	cout << "Testing that the babysitter cannot end a shift before beginning one at times after midnight\n";
	Babysitter b;

	b.setStartTime(1);
	b.setEndTime(0);

	assert(b.verifyEndTime() == false);
	cout << "Test 'theBabysitterCannotLeaveBeforeStartingWithValidTimesAfterMidnight' passed !\n\n";
}

void theBabysitterCannotLeaveBeforeStartingWithValidTimesBeforeAndAfterMidnight()
{
	cout << "----Test 'theBabysitterCannotLeaveBeforeStartingWithValidTimesBeforeAndAfterMidnight'----\n";
	cout << "Testing that the babysitter cannot end a shift before beginning one at times after midnight\n";
	Babysitter b;

	b.setStartTime(1);
	b.setEndTime(17);

	assert(b.verifyEndTime() == false);
	cout << "Test 'theBabysitterCannotLeaveBeforeStartingWithValidTimesBeforeAndAfterMidnight' passed !\n\n";
}

void twentyThreeIsAnAcceptableTimeInputValue()
{
	cout << "----Test 'twentyThreeIsAnAcceptableTimeInputValue'----\n";
	cout << "Testing that the babysitter can input 23 as a start or end time\n";
	Babysitter b;

	assert (b.timeInputIsBetweenZeroAndTwentyThree(23) == true);

	cout << "Test 'twentyThreeIsAnAcceptableTimeInputValue' passed !\n\n";
}

void twentyFourIsNotAnAcceptableTimeInputValue()
{
	cout << "----Test 'twentyFourIsNotAnAcceptableTimeInputValue'----\n";
	cout << "Testing that the babysitter cannot input 24 as a start or end time\n";
	Babysitter b;

	assert (b.timeInputIsBetweenZeroAndTwentyThree(24) == false);

	cout << "Test 'twentyFourIsNotAnAcceptableTimeInputValue' passed !\n\n";
}

void numbersGreaterThanTwentyThreeAreInvalidTimeInputs()
{
	cout << "----Test 'numbersGreaterThanTwentyThreeAreInvalidTimeInputs'----\n";
	cout << "Testing that the babysitter cannot input numbers greater than 23 as a start or end time\n";
	Babysitter b;

	for (int i = 24; i < 50; ++i)
		assert (b.timeInputIsBetweenZeroAndTwentyThree(i) == false);

	cout << "Test 'numbersGreaterThanTwentyThreeAreInvalidTimeInputs' passed !\n\n";
}

void zeroIsAnAcceptableTimeInputValue()
{
	cout << "----Test 'zeroIsAnAcceptableTimeInputValue'----\n";
	cout << "Testing that the babysitter can input 0 as a start or end time\n";
	Babysitter b;

	assert (b.timeInputIsBetweenZeroAndTwentyThree(0) == true);

	cout << "Test 'zeroIsAnAcceptableTimeInputValue' passed !\n\n";	
}

void negativeOneIsNotAnAcceptableTimeInputValue()
{
	cout << "----Test 'negativeOneIsNotAnAcceptableTimeInputValue'----\n";
	cout << "Testing that the babysitter cannot input -1 as a start or end time\n";
	Babysitter b;

	assert (b.timeInputIsBetweenZeroAndTwentyThree(-1) == false);

	cout << "Test 'negativeOneIsNotAnAcceptableTimeInputValue' passed !\n\n";	
}

void numbersLessThanZeroAreNotAnAcceptableTimeInputValues()
{
	cout << "----Test 'numbersLessThanZeroAreNotAnAcceptableTimeInputValues'----\n";
	cout << "Testing that the babysitter cannot input numbers less than 0 as a start or end time\n";
	Babysitter b;

	for (int i = -1; i > -50; --i)
		assert (b.timeInputIsBetweenZeroAndTwentyThree(i) == false);

	cout << "Test 'numbersLessThanZeroAreNotAnAcceptableTimeInputValues' passed !\n\n";	
}

void theBabyistterCanStartAtFivePMAndEndAtOneAM()
{
	cout << "----Test 'theBabyistterCanStartAtFivePMAndEndAtOneAM'----\n";
	cout << "Testing that the babysitter can start at 5:00PM and end at 1:00AM\n";
	Babysitter b;
	
	b.setStartTime(17);
	b.setEndTime(1);

	assert(b.verifyStartTime() == true);
	assert(b.verifyEndTime() == true);

	cout << "Test 'theBabyistterCanStartAtFivePMAndEndAtOneAM' passed !\n\n";		
}

void theBabyistterCannotStartAtOneAMAndEndAtFivePM()
{
	cout << "----Test 'theBabyistterCannotStartAtOneAMAndEndAtFivePM'----\n";
	cout << "Testing that the babysitter cannot start at 1:00AM and end at 5:00PM\n";
	Babysitter b;
	
	b.setStartTime(1);
	b.setEndTime(17);

	assert(b.verifyStartTime() == true);
	assert(b.verifyEndTime() == false);

	cout << "Test 'theBabyistterCannotStartAtOneAMAndEndAtFivePM' passed !\n\n";		
}

void theBabyistterCannotStartAtTwoAMAndEndAtOneAM()
{
	cout << "----Test 'theBabyistterCannotStartAtTwoAMAndEndAtOneAM'----\n";
	cout << "Testing that the babysitter cannot start at 2:00AM and end at 1:00AM\n";
	Babysitter b;
	
	b.setStartTime(2);
	b.setEndTime(1);

	assert(b.verifyStartTime() == true);
	assert(b.verifyEndTime() == false);

	cout << "Test 'theBabyistterCannotStartAtTwoAMAndEndAtOneAM' passed !\n\n";		
}

void theBabyistterCanStartAtOneAMAndEndAtTwoAM()
{
	cout << "----Test 'theBabyistterCanStartAtOneAMAndEndAtTwoAM'----\n";
	cout << "Testing that the babysitter can start at 1:00AM and end at 2:00AM\n";
	Babysitter b;
	
	b.setStartTime(1);
	b.setEndTime(2);

	assert(b.verifyStartTime() == true);
	assert(b.verifyEndTime() == true);

	cout << "Test 'theBabyistterCanStartAtOneAMAndEndAtTwoAM' passed !\n\n";		
}

void theBabyistterCanStartAtFivePMAndEndAtSixPM()
{
	cout << "----Test 'theBabyistterCanStartAtFivePMAndEndAtSixPM'----\n";
	cout << "Testing that the babysitter can start at 5:00PM and end at 6:00PM\n";
	Babysitter b;
	
	b.setStartTime(17);
	b.setEndTime(18);

	assert(b.verifyStartTime() == true);
	assert(b.verifyEndTime() == true);

	cout << "Test 'theBabyistterCanStartAtFivePMAndEndAtSixPM' passed !\n\n";		
}

void theBabyistterCannotStartAtSixPMAndEndAtFivePM()
{
	cout << "----Test 'theBabyistterCannotStartAtSixPMAndEndAtFivePM'----\n";
	cout << "Testing that the babysitter cannot start at 6:00PM and end at 5:00PM\n";
	Babysitter b;
	
	b.setStartTime(18);
	b.setEndTime(17);

	assert(b.verifyStartTime() == true);
	assert(b.verifyEndTime() == false);

	cout << "Test 'theBabyistterCannotStartAtSixPMAndEndAtFivePM' passed !\n\n";		
}

void workingFromFivePMToOneAMYieldsEightyFourDollars()
{
	cout << "----Test 'workingFromFivePMToOneAMYieldsEightyFourDollars'----\n";
	cout << "Testing that the babysitter works from 5:00PM to 1:00AM and makes $84\n";
	Babysitter b;

	b.setStartTime(17);
	b.setEndTime(1);

	assert(b.calculateEarnings() == 84);

	cout << "Test 'workingFromFivePMToOneAMYieldsEightyFourDollars' passed !\n\n";		
}

void workingFromFivePMToThreeAMYieldsOneHundredAndSixteenDollars()
{
	cout << "----Test 'workingFromFivePMToThreeAMYieldsOneHundredAndSixteenDollars'----\n";
	cout << "Testing that the babysitter works from 5:00PM to 3:00AM and makes $116\n";
	Babysitter b;

	b.setStartTime(17);
	b.setEndTime(3);

	assert(b.calculateEarnings() == 116);

	cout << "Test 'workingFromFivePMToThreeAMYieldsOneHundredAndSixteenDollars' passed !\n\n";		
}

void workingFromMidnightToThreeAMYieldsFortyEightDollars()
{
	cout << "----Test 'workingFromMidnightToThreeAMYieldsFortyEightDollars'----\n";
	cout << "Testing that the babysitter works from 12:00AM to 3:00AM and makes $48\n";
	Babysitter b;

	b.setStartTime(0);
	b.setEndTime(3);

	assert(b.calculateEarnings() == 48);

	cout << "Test 'workingFromMidnightToThreeAMYieldsFortyEightDollars' passed !\n\n";		
}

void workingFromThreeAMToThreeAMYieldsZeroDollars()
{
	cout << "----Test 'workingFromThreeAMToThreeAMYieldsZeroDollars'----\n";
	cout << "Testing that the babysitter works from 3:00AM to 3:00AM and makes $0\n";
	Babysitter b;

	b.setStartTime(3);
	b.setEndTime(3);

	assert(b.calculateEarnings() == 0);

	cout << "Test 'workingFromThreeAMToThreeAMYieldsZeroDollars' passed !\n\n";		
}

int main()
{
	cout << "\n";

	theBabysitterCanStartAtFivePM();
	theBabysitterCannotStartAtFourPM();
	theBabysitterCannotStartBetweenFiveAMAndFourPM();
	theBabysitterCanStartBetweenFivePMAndFourAM();

	theBabysitterCanLeaveAtFourAM();
	theBabysitterCannotLeaveAtFiveAM();
	theBabysitterCannotLeaveBetweenFiveAMAndFourPM();
	theBabysitterCanLeaveBetweenFivePMAndFourAM();

	theBabysitterCannotLeaveBeforeStartingWithValidTimesBeforeMidnight();
	theBabysitterCannotLeaveBeforeStartingWithValidTimesAfterMidnight();
	theBabysitterCannotLeaveBeforeStartingWithValidTimesBeforeAndAfterMidnight();

	twentyThreeIsAnAcceptableTimeInputValue();
	twentyFourIsNotAnAcceptableTimeInputValue();
	zeroIsAnAcceptableTimeInputValue();
	negativeOneIsNotAnAcceptableTimeInputValue();

	/*--------------began writing driver----------------------*/

	theBabyistterCanStartAtFivePMAndEndAtOneAM();
	theBabyistterCannotStartAtOneAMAndEndAtFivePM();
	theBabyistterCannotStartAtTwoAMAndEndAtOneAM();
	theBabyistterCanStartAtOneAMAndEndAtTwoAM();
	theBabyistterCanStartAtFivePMAndEndAtSixPM();
	theBabyistterCannotStartAtSixPMAndEndAtFivePM();

	workingFromFivePMToOneAMYieldsEightyFourDollars();
	workingFromFivePMToThreeAMYieldsOneHundredAndSixteenDollars();
	workingFromMidnightToThreeAMYieldsFortyEightDollars();
	workingFromThreeAMToThreeAMYieldsZeroDollars();

	cout << "\nAll tests passed !\n\n";
	return 0;
}