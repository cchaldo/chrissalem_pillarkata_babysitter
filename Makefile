CXX=g++

default: Babysitter.h Babysitter.cpp driver.cpp
	$(CXX) Babysitter.cpp driver.cpp -o babysitter

run: default
	./babysitter

tests: default tests.cpp
	$(CXX) Babysitter.cpp tests.cpp -o tests

runtests: tests
	./tests

clean:
	rm babysitter tests